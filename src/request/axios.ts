import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
// 公共状态文件
import { common } from "/@/hooks/common";
// 引入公共函数js文件
import utils from "/@/assets/js/public/function";

if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
{
  // 默认请求连接
  axios.defaults.baseURL = import.meta.env.VITE_APP_PHP_URL;
}
else
{
  // 默认请求连接
  axios.defaults.baseURL = import.meta.env.VITE_APP_JAVA_URL;
}



// 超时时间（ms）
axios.defaults.timeout = 2000 * 1000;
// axios请求开启cookie，支持跨域请求携带cookie
// axios.defaults.withCredentials = true;
// axios 请求头
// axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'
// axios.defaults.headers['Access-Control-Allow-Origin'] = '*'
// axios.defaults.headers['Access-Control-Allow-Methods'] = '*'
// axios.defaults.headers['Access-Control-Allow-Headers'] = '*'
// axios.defaults.headers['Accept'] = '*/*'
axios.defaults.headers["token"] = localStorage.getItem("token") || ""
axios.defaults.headers["satoken"] = localStorage.getItem("satoken") || ""
// axios.defaults.headers.post['Content-Type'] = 'application/json'
// post头
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";

// 请求拦截
axios.interceptors.request.use(
  (config: AxiosRequestConfig) => { 
    // 可在这里做一些数据的校验。
    // session的校验等。
    return config 
  },
  (error: AxiosError) => { 
    return error 
  }
)

// 响应拦截
axios.interceptors.response.use((result: AxiosResponse) => {
  // ===========================================================
  // 返回方式一
  /*console.log(result);
  if (result.status === 200) {
    if (result.data && result.data.code > 0) {
      return Promise.resolve(result);
    } else {
      alert(result.data.msg || "操作失败");
      return Promise.reject(result);
    }
  } else {
    alert("网络异常");
    return Promise.reject(result);
  }//*/

  // ==========================================================
  // 返回方式二
  // 返回数据前做了什么
  // console.log(result);
  if (result.data.code < -100) 
  {
    if (result.data.msg)
    {
      // 调用自定义alert
      utils.alert(result.data.msg, function () {
        // 判断手机端/pc端，跳路由
        let result = utils.IsPC();
        if(result)
        {
          window.location.assign('/pc/index');
        }
        else
        {
          window.location.assign('/phone/index');
        }
      });
    }
    return Promise.reject(result.data.data)
  }
  return result;
}, (err: AxiosError) => {
  utils.alertLoadExec(false);
  // 返回数据前做了什么
  return Promise.reject(err)
})
export default axios