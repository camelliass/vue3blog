// 引入公共js文件
import request from "/@/request/request";

/**
 * @name:获取手机端首页数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getIndexData = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getIndexData", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getIndexData", data, '');
  }
}

/**
 * @name:获取闲言碎语数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getRemarkList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getRemarkList", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getRemarkList", data, '');
  }
}

/**
 * @name:获取标签列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getFileList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getFileList", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getFileList", data, '');
  }
}

/**
 * @name:获取文章详情
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getArticleDetail = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getArticleDetail", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getArticleDetail", data, '');
  }
}

/**
 * @name:获取用户登录信息
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getUserSession = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getUserSession", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getUserSession", data, '');
  }
}

/**
 * @name:获取邮箱验证码
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getEmailCode = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getEmailCode", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getEmailCode", data, '');
  }
}

/**
 * @name:账号密码登录/注册
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const accountLogin = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/accountLogin", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/accountLogin", data, '');
  }
}

/**
 * @name: 获取留言列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const getMessageList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/phoneIndex/getMessageList", data, '');
  }
  else
  {
    return request.get("/java/phoneIndex/getMessageList", data, '');
  }
}

/**
 * @name: 登出
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const layout = (data: any) => request.get("/index.php/phoneIndex/layout", data, '');

/**
 * @name: 文章评论
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const putComment = (data: any) => request.get("/index.php/phoneIndex/putComment", data, '');

/**
 * @name: 留言
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-03-01 
 */
export const putMessage = (data: any) => request.get("/index.php/phoneIndex/putMessage", data, '');

