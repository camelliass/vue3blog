// 引入公共js文件
import request from "/@/request/request";
/**
 * @name:提交友情链接
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getFileList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/placeonfile/getFileList", data, '');
  }
  else
  {
    return request.get("/java/placeonfile/getFileList", data, '');
  }
}


