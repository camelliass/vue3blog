// 引入公共js文件
import request from "/@/request/request";

/**
 * @name:获取友情链接列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getLabelList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/label/getLabelList", data, '');
  }
  else
  {
    return request.get("/java/label/getLabelList", data, '');
  }
}

