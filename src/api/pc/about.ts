// 引入公共js文件
import request from "/@/request/request";

/**
 * @name: 提交留言
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const putmessage = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/about/putmessage", data, '');
  }
  else
  {
    return request.get("/java/about/putmessage", data, '');
  }
}


/**
 * @name: 获取留言列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getMessgetList = (data: any) => {
  // import.meta.env.VITE_APP_LANGUAGE
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/about/getMessgetList", data, '');
  }
  else
  {
    return request.get("/java/about/getMessgetList", data, '');
  }
}