// 引入公共js文件
import request from "/@/request/request";

/**
 * @name: 提交留言
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const putComment = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/article/putComment", data, '');
  }
  else
  {
    return request.get("/java/article/putComment", data, '');
  }
}

/**
 * @name: 获取留言列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getArticleDetail = (data: any) =>
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/article/getArticleDetail", data, '');
  }
  else
  {
    return request.get("/java/article/getArticleDetail", data, '')
  }
}

