// 引入公共js文件
import request from "/@/request/request";

/**
 * @name: 获取数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getdata = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/tools/getData", data, '');
  }
  else
  {
    return request.get("/java/tools/getData", data, '');
  }
}

/**
 * @name: excel数据统计
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const clickOKExcel = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/user/clickOKExcel", data, '');
  }
  else
  {
    return request.get("/java/tools/getData", data, '');
  }
}

