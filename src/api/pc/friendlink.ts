// 引入公共js文件
import request from "/@/request/request";

/**
 * @name:提交友情链接
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const applyFriendLink = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.post("/index.php/friendlink/applyFriendLink", data, '');                            
  }
  else
  {
    return request.post("/java/friendlink/applyFriendLink", data, '');
  }
}

/**
 * @name:获取友情链接列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-01 
 */
export const getFriendLinkList = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/friendlink/getFriendLinkList", data, '');                         
  }
  else
  {
    return request.get("/java/friendlink/getFriendLinkList", data, '');
  }
}


