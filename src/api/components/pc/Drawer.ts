// 引入公共js文件
import request from "/@/request/request";
/**
 * @name: 获取底部信息及session信息
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 
 */
export const getChatRecordApi = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/chat/getChatRecord", data, '');
  }
  else
  {
    return request.get("/java/chat/getChatRecord", data, '');
  }
}

