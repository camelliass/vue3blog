// 引入公共js文件
import request from "/@/request/request";
/**
 * @name: 账号密码登录/注册/更改密码
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 
 */
export const accountLogin = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.post("/index.php/user/accountLogin", data, '');
  }
  else
  {
    return request.get("/java/login/accountLogin", data, '');
  }
}

/**
 * @name: 退出登录
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 22:07:58
 */
export const layout = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/user/layout", data, '');
  }
  else
  {
    return request.get("/java/login/layout", data, '');
  }
}


/**
 * @name: 获取底部信息及session信息
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 
 */
export const getFooterDataApi = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.get("/index.php/footer/getFooterData", data, '')
  }
  else
  {
    return request.get("/java/footer/getFooterData", data, '')
  }
}

/**
 * @name: 修改昵称
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 
 */
export const updateNicknameApi = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.post("/index.php/user/updateNickname", data, '');
  }
  else
  {
    return request.post("/java/login/updateNickname", data, '');
  }
}

/**
 * @name: 发邮件验证码
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-03-08 22:22:14
 */
export const sendEmailCodeApi = (data: any) => 
{
  if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
  {
    return request.post("/index.php/user/sendEmailCode", data, '');
  }
  else
  {
    return request.post("/java/login/sendEmailCode", data, '');
  }
}

