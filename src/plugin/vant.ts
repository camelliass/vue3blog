/**
 * @name: vant 组件全局加载文件
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2020-12-30 15:06:19
 */
// This module only introduces components globally before login
import type { App } from 'vue';

import {
    // need
    Form, Field, CellGroup, Button, Checkbox, CheckboxGroup,
    Grid, GridItem, NavBar,
    Tabbar, TabbarItem,
    Cell, Card,
    Calendar, Empty,
    Icon, Loading, Toast, SwipeCell, Dialog, Popup, Picker,
    Row,
    Col, Search, Uploader, PullRefresh, List, RadioGroup, Radio,Tab,Tabs,
} from 'vant';
import 'vant/lib/index.css';//*/

export function setupVant(app: App<Element>) {
    // need
    // Here are the components required before registering and logging in
    app.use(Form)
        .use(RadioGroup)
        .use(Tab)
        .use(Tabs)
        .use(Radio)
        .use(Field)
        .use(CellGroup)
        .use(Button)
        .use(Checkbox)
        .use(CheckboxGroup)
        .use(Grid)
        .use(GridItem)
        .use(NavBar)
        .use(Tabbar)
        .use(TabbarItem)
        .use(Icon)
        .use(Cell)
        .use(Card)
        .use(Calendar)
        .use(Empty)
        .use(Loading)
        .use(Toast)
        .use(SwipeCell)
        .use(Dialog)
        .use(Popup)
        .use(Picker)
        .use(Row)
        .use(Col)
        .use(Search)
        .use(Uploader)
        .use(PullRefresh)
        .use(List)
}//*/