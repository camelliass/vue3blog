/**
 * @name: antd 组件全局加载文件
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-12-30 
 */
// This module only introduces components globally before login
import type { App } from 'vue';

import {
    Form,
    Image,
    Input,
    Row,
    Col,
    Spin,
    Button,
    Menu,
    Space,
    Pagination,
    DatePicker,
    Checkbox,
    Radio,
    Switch,
    TimePicker,
    TreeSelect,
    Upload,
    Avatar,
    Card,
    Empty,
    Table,
    Layout,
    List,
    Tabs,
    Tag,
    Tree,
    Modal,
    message,
    Drawer,
    Progress,
    Tooltip,
    Collapse,
    Transfer,
    Select,
    Dropdown,
    Statistic,
    Breadcrumb,
    Divider,
    Timeline
} from 'ant-design-vue';

export function setupAntd(app: App<Element>) {
    // need
    // Here are the components required before registering and logging in
    app.use(Form)
        .use(Image)
        .use(Input)
        .use(Row)
        .use(Col)
        .use(Spin)
        .use(Menu)
        .use(Space)
        .use(Pagination)
        .use(DatePicker)
        .use(Checkbox)
        .use(Radio)
        .use(Switch)
        .use(TimePicker)
        .use(TreeSelect)
        .use(Upload)
        .use(Avatar)
        .use(Card)
        .use(Empty)
        .use(Table)
        .use(Layout)
        .use(List)
        .use(Tabs)
        .use(Tag)
        .use(Tree)
        .use(Modal)
        .use(Drawer)
        .use(Progress)
        .use(Tooltip)
        .use(Collapse)
        .use(Transfer)
        .use(Select)
        .use(Dropdown)
        .use(Statistic)
        .use(Breadcrumb)
        .use(Divider)
        .use(Timeline)
        .use(Button);
}//*/

// 全局引入
// import 'ant-design-vue/dist/antd.css';//*/
// import 'ant-design-vue/es/date-picker/style/css'; //vite只能用 ant-design-vue/es 而非 ant-design-vue/lib