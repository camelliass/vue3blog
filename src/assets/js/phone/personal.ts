import {
    PropType,
    ref,
    watch,
    reactive,
    toRefs,
    provide,
    inject,
    onBeforeMount, // 在组件挂载之前执行的函数
    onMounted,
    onBeforeUpdate, // 在组件修改之前执行的函数
    onUpdated,
    onBeforeUnmount, // 在组件卸载之前执行的函数
    onUnmounted,
    nextTick,
} from "vue";
// 引入路由
import { useRouter, useRoute } from "vue-router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// 引入子组件
import Menu from "/@/components/phone/Menu.vue";
// api 接口文件
import {
    getUserSession,
    getEmailCode,
    accountLogin,
    getMessageList,
    layout,
    putMessage,
} from "/@/api/phone/index";
import { common, userinfo } from "/@/hooks/common";
import { Toast } from "vant";

/**
 * @name: 声明data
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
const data = reactive({
    // 留言列表
    messageList: Array<any>(),
    // 标签索引
    // active: <number>0,
    // 用户名（邮箱）
    username: <string>"",
    // 密码
    password: <string>"",
    // 密码
    password_again: <string>"",
    // 验证码
    smsCode: <string>"",
    // 按钮状态
    buttonStatus: <boolean>false,
    // 按钮文字
    buttonWord: <string>"发送验证码",
    // 按钮倒计时
    buttonNumber: <number>60,
    // 登出按钮显示文字
    buttonLogout: <string>"",
    // 头像链接
    figureurl: <string>userinfo.figureurl,
    // 文本域输入值
    textValue: <string>"",
});

/**
 * @name: 将data绑定值dataRef
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-10
 */
export const {
    messageList,
    // active,
    username,
    password,
    password_again,
    smsCode,
    buttonStatus,
    buttonWord,
    buttonLogout,
    figureurl,
    textValue,
} = toRefs(data);

/**
 * @name: 监听登录状态变化
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-01-20
 */
watch(
    () => userinfo.userid,
    () => {
        data.buttonLogout = userinfo.userid ? "登出" : "";
        data.figureurl = userinfo.figureurl ? userinfo.figureurl : "";
    }
);

// ===============================================================================
// 子组件相关
// 子组件ref（TypeScript语法）
export const MenuRef = ref<InstanceType<typeof Menu>>();

// ===============================================================================
// 方法
/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    const active = ref<number>(0);

    onMounted( async()=>{
        // 执行子组件方法
        const {actives} = await MenuRef.value?.getPersonalData();
        actives.value = active.value;
        // =================================================================
        getUserSessionData();
    });
    return {MenuRef,active};
}

/**
 * @name: 留言
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const putMessageExec = () => {
    if (data.textValue == "") {
        Toast("请填写留言内容！");
        return;
    }
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    let param = { content: data.textValue };
    putMessage(param).then(function (response: any) {
        console.log(response);
        data.textValue = "";
        utils.sleep(1000).then(() => {
            // 这里写sleep之后需要去做的事情
            // 回到滚动条刷新前位置
            Toast.clear();
        });
        Toast(response.msg);
    }); //*/
};

/**
 * @name: 点击登出按钮
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const onClickRight = () => {
    if(import.meta.env.VITE_APP_LANGUAGE == 'PHP')
    {
        let param = {};
        layout(param).then(function (resp: any) {
            userinfo.userid = "";
            userinfo.figureurl = "";
            userinfo.nickname = "";
            userinfo.email = "";
        });
    }
    else
    {
        localStorage.setItem("token","");
        getUserSessionData();
    }
    

};

/**
 * @name: 获取 邮箱 验证码
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const getSmsCode = () => {
    if (data.username == "") {
        Toast("请输入邮箱");
        return;
    }
    let param = { email: data.username };
    data.buttonStatus = true;
    getEmailCode(param).then(function (response: any) {
        console.log(response);
        var timer = setInterval(function () {
            data.buttonNumber -= 1;
            data.buttonWord = "发送验证码 ( " + data.buttonNumber + " ) ";
            if (data.buttonNumber == 0) {
                data.buttonWord = "发送验证码";
                data.buttonStatus = false;
                data.buttonNumber = 60;
                clearInterval(timer);
            }
        }, 1000);
    });
};

/**
 * @name: 子组件向父组件抛出的方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const getPersonalData = (response: any): void => { };

/**
 * @name: 获取用户登录信息
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const getUserSessionData = (): void => {
    let param = {};
    getUserSession(param).then(function (response: any) {
        userinfo.userid = response.userid;
        userinfo.figureurl = response.figureurl;
        userinfo.nickname = response.nickname;
        userinfo.email = response.email;
        // 如果登陆了，获取一下留言列表
        if (userinfo.userid) {
            Toast.loading({
                message: "加载中...",
                forbidClick: true,
            });
            getMessageList(param).then(function (resp: any) {
                data.messageList = resp.messageList;
                utils.sleep(1000).then(() => {
                    // 这里写sleep之后需要去做的事情
                    // 回到滚动条刷新前位置
                    Toast.clear();
                });
            });
        }
    });
};

/**
 * @name: 登录
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const onSubmitLogin = (): void => {
    if (data.username == "" || data.password == "") {
        Toast("请输入完整信息！");
        return;
    }
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    let params = { email: data.username, password: data.password, sign: "login" };
    let param = { params: utils.encryptCode(params) };
    accountLogin(param).then(function (response: any) {
        userinfo.userid = response.userid;
        userinfo.figureurl = response.figureurl;
        userinfo.nickname = response.nickname;
        userinfo.email = response.email;
        // 存储token
        localStorage.setItem("token",response.token);
        getMessageList(param).then(function (resp: any) {
            data.messageList = resp.messageList;
        });
        utils.sleep(1000).then(() => {
            // 这里写sleep之后需要去做的事情
            Toast.clear();
        });
    });
};

/**
 * @name: 注册
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const onSubmitRegister = (): void => {
    if (
        data.username == "" ||
        data.password == "" ||
        data.password_again == "" ||
        data.smsCode == ""
    ) {
        Toast("请输入完整信息！");
        return;
    }
    if (data.password != data.password_again) {
        Toast("两次输入密码不一致！");
        return;
    }
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    let params = {
        email: data.username,
        password: data.password,
        sign: "register",
        emailCode: data.smsCode,
    };
    let param = { params: utils.encryptCode(params) };
    accountLogin(param).then(function (response: any) {
        userinfo.userid = response.userid;
        userinfo.figureurl = response.figureurl;
        userinfo.nickname = response.nickname;
        userinfo.email = response.email;
        // 存储token
        localStorage.setItem("token",response.token);
        utils.sleep(1000).then(() => {
            // 这里写sleep之后需要去做的事情
            Toast.clear();
        });
    });
};

