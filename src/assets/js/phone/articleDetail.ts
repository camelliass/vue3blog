import {
    PropType,
    ref,
    watch,
    reactive,
    toRefs,
    provide,
    inject,
    onBeforeMount, // 在组件挂载之前执行的函数
    onMounted,
    nextTick,
} from "vue";
//导入代码高亮文件
import hljs from "highlight.js";
// 引入路由
import { useRouter, useRoute } from "vue-router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// api 接口文件
import { getArticleDetail, getUserSession, putComment } from "/@/api/phone/index";
import { Toast } from "vant";
import { common, userinfo } from "/@/hooks/common";

// =============================================================================
// 实例化路由
const router = useRouter();
const route = useRoute();

/**
 * @name: 声明data
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
const data = reactive({
// 文章内容
articleData: Array<any>(["artlogo", "arttitle", "putime", "click_num", "content"]),
// 文章标题
articleTitle: <Number>0,
// 点击数量
click_num: <Number>0,
// 发布时间
putime: <String>"",
// 文章id
article_id: <any>(0),
// 来源
path: <any>("index"),
// 上一篇
front: Array<any>(),
// 下一篇
after: Array<any>(),
// 是否登录标识
loginSign: userinfo.userid ? true : false,
// 文本域输入值
textValue: <String>"",
});

/**
 * @name: 将data绑定值dataRef
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-01-10
*/
export const {
    articleTitle,
    click_num,
    putime,
    articleData,
    front,
    after,
    loginSign,
    textValue,
} = toRefs(data);

/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    onMounted( ()=>{
        // 实例化路由
        var router = useRouter();
        // 路由参数
        var route = useRoute();
        data.article_id = route.query.article_id ;
        data.path = route.query.path ;

    });
}

/**
 * @name: 监听登录状态变化
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2021-01-20
*/
watch(
    () => userinfo.userid,
    () => {
        data.loginSign = userinfo.userid ? true : false;
    }
);

// ===============================================================================
// 方法

/**
 * @name: 文章评论
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
export const putCommentExec = () => {
    if (data.textValue == "") {
        Toast("请填写评论内容！");
        return;
    }
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    let param = { article_id: data.article_id, content: data.textValue };
    putComment(param).then(function (response: any) {
        // console.log(response);
        data.textValue = "";
        utils.sleep(1000).then(() => {
        // 这里写sleep之后需要去做的事情
        // 回到滚动条刷新前位置
        Toast.clear();
        });
        Toast(response.msg);
    }); //*/
};

/**
 * @name: 跳个人中心页
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
export const jumpPersonal = () => {
    router.push("/phone/personal");
};

/**
 * @name: 返回上一页
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
export const onClickLeft = () => {
    router.push("/phone/" + data.path);
};

/**
 * @name: 跳转文章详情页
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
* @param:	category_id	number	分类
*/
export const jumpArticleDetail = (article_id: Number) => {
    getArticleDetailData(utils.encryptCode({ article_id: article_id }));
};

/**
 * @name: 获取文章详情数据
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
export const getArticleDetailData = (article_id: any): void => {
    try {
        Toast.loading({
        message: "加载中...",
        forbidClick: true,
        });
        let param = { article_id: article_id };
        getArticleDetail(param).then(function (response: any) {
        data.articleData = response.detail;
        // data.articleData.content = hlCode(response.detail.content);
        data.front = response.front;
        data.after = response.after;
        // 滚动条回顶部
        let json = document.getElementById("body");
        if (json != null) {
            json.scrollTop = 0;
        }

        utils.sleep(3000).then(() => {
            // 这里写sleep之后需要去做的事情
            // 回到滚动条刷新前位置
            Toast.clear();
        });
        });
    } catch (error) {
        // console.log('chucuole');
        // 自定义loading消失
        // utils.alertLoadExec(false);
        Toast.clear();
    }   
};

/**
 * @name: editor代码块标签高亮显示
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2021-01-19 21:06:22
 */
export const hlCode = (content: string) => {
    //替换pre标签，前端高亮显示
    let c = document.createElement("div");
    c.innerHTML = content;
    for (let i = 0; i < c.getElementsByTagName("pre").length; i++) {
        let con = c.getElementsByTagName("pre")[i].innerHTML;
        c.getElementsByTagName("pre")[i].innerHTML =
        '<code class="hljs ini" style="word-break: break-all;">' + con + "</code>";
        hljs.highlightBlock(c.getElementsByTagName("pre")[i]); //使用插件高亮你的代码块
    }
    content = c.innerHTML;
    return content;
};

// /**
//  * @name: 显示大图
//  * @author: camellia
//  * @email: guanchao_gc@qq.com
//  * @date: 2021-03-10 22:37:32
//  * @param:	data	type	description
//  */
// const imageBoost = (str:string) => {
//     data.imgSrc = str;
//     data.showImg = true;
//     common.bigImgShow = data.showImg;
// }
// window.imageBoost = imageBoost;

/**
 * @name: 获取用户登录信息
* @author: camellia
* @email: guanchao_gc@qq.com
* @date: 2022-07-18
*/
export const getUserSessionData = (): void => {
    try {
        let param = {};
        getUserSession(param).then(function (response: any) {
            userinfo.userid = response.userid;
            userinfo.figureurl = response.figureurl;
            userinfo.nickname = response.nickname;
            userinfo.email = response.email;
            if (userinfo.userid) 
            {
                data.loginSign = true;
            }
        });
    } catch (error) {
        console.log("登录信息出错！");
    }
};

// ===============================================================================
// 获取登录状态
getUserSessionData();
// 获取文章详情数据
getArticleDetailData(data.article_id);