import {
    ref,
    watch,
    reactive,
    toRefs,
    onMounted,
    nextTick,
} from "vue";
// 引入路由
import router from "/@/router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// 引入子组件
import Menu from "/@/components/phone/Menu.vue";

/**
 * @name: 声明data
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-18
 */
const data = reactive({
    // 文章列表
    dataList: Array<any>(),
    // 文章数量
    articleCount: <Number>0,
});

/**
 * @name: 将data绑定值dataRef
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-10
 */
export const { dataList, articleCount } = toRefs(data);

// ===============================================================================
// 子组件相关
/**
 * @name: 定义子组件暴露给父组件的值属性列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
interface menuData {
    goIndex: () => void;
    getIndexDataList: () => void;
    goOther: () => void;
    homeColor: string;
    otherColor: string;
    placeFileColor: string;
    personalColor: string;
}
// 子组件ref（TypeScript语法）下面这这两种写法也可以，推荐使用Typescript语法
export const MenuRef = ref<InstanceType<typeof Menu> & menuData>();
// const MenuRef = ref<InstanceType<typeof Menu>>()
// const MenuRef = ref(null)

// ===============================================================================
// 方法

/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    const dataList = ref(Array<any>());
    const articleCount = ref(Array<any>());
    const active = ref<number>(0);
    onMounted( async()=>{
        // 执行子组件方法
        const {result,actives} = await MenuRef.value?.getPlaceFileData();
        // console.log(result.value);
        dataList.value = result.value.dataList;
        articleCount.value = result.value.articleCount;
        active.value = actives.value;
    });
    return {MenuRef,dataList,active,articleCount};
}

/**
 * @name: 跳转文章详情页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 * @param:	category_id	number	分类
 */
export const jumpArticleDetail = (article_id: Number) => {
    router.push({
        path: "/phone/articleDetail",
        query: {
            article_id: utils.encryptCode({ article_id: article_id }),
            path: "placeFile",
        },
    });
};
/**
 * @name: 子组件向父组件抛出的方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const goPlaceFile = (response: any): any => {
    console.log(response)
    const dataList = ref(Array<any>());
    const articleCount = ref(Array<any>());
    try {
        // 数组赋值
        dataList.value = response.dataList;
        articleCount.value = response.articleCount;
    } catch (error) {
        console.log("出错了");
    }
    return {dataList,articleCount}
};

/**
 * @name: 生命周期---页面挂载完成
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
const mounted = onMounted(() => {
    // console.log('获取子组件中的性别', MenuRef );
    // console.log('获取子组件中的性别', MenuRef.value );
    // console.log('获取子组件中的性别', MenuRef.value.sex );
    // console.log('获取子组件中的其他信息', MenuRef.value.info );
    // console.log('获取子组件中的其他信息', MenuRef.value.homeColor );
    // 执行子组件方法
    MenuRef.value?.getPlaceFileData();
});