import {
    ref,
    watch,
    reactive,
    toRefs,
    onMounted,
    nextTick,
} from "vue";
// 引入路由
import { useRouter, useRoute } from "vue-router";
import { active } from "./personal";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// 引入子组件
import Menu from "/@/components/phone/Menu.vue";

// ===============================================================================
// 子组件相关
/**
 * @name: 定义子组件暴露给父组件的值属性列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
interface menuData {
    goIndex: () => void;
    getIndexDataList: () => void;
    goOther: () => void;
    homeColor: string;
    otherColor: string;
    placeFileColor: string;
    personalColor: string;
}
// 子组件ref（TypeScript语法）下面这这两种写法也可以，推荐使用Typescript语法
export const MenuRef = ref<InstanceType<typeof Menu> & menuData>();
// const MenuRef = ref<InstanceType<typeof Menu>>()
// const MenuRef = ref(null)

// ===============================================================================
// 方法
/**
 * @name: 子组件向父组件抛出的方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const goOther = (response: any): any => {
    const dataList = ref(Array<any>());
    try {
        // 数组赋值
        dataList.value = response;
    } 
    catch (error) 
    {
        console.log("出错了");
    }
    return dataList;
};

/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    const dataList = ref(Array<any>());
    const active = ref<number>(0);
    onMounted( async()=>{
        // 执行子组件方法
        const {result,actives} = await MenuRef.value?.getOtherData();
        // console.log(result.value);
        dataList.value = result.value;
        active.value = actives.value;
    });
    return {MenuRef,dataList,active};
}
