import {
    PropType,
    ref,
    watch,
    reactive,
    toRefs,
    provide,
    inject,
    onBeforeMount, // 在组件挂载之前执行的函数
    onMounted,
    nextTick,
} from "vue";

// 引入路由
import router from "/@/router";
// 引入路由
import { useRouter, useRoute } from "vue-router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// 引入子组件
import Menu from "/@/components/phone/Menu.vue";
import { common, userinfo } from "/@/hooks/common";
import { Toast } from "vant";

// =============================================================================
// 实例化路由
// const router = useRouter();
// const route = useRoute();

/**
 * @name: 声明data
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-18
 */
const data = reactive({});

/**
 * @name: 将data绑定值dataRef
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-10
 */
export const {} = toRefs(data);

// ===============================================================================
// 子组件相关
/**
 * @name: 定义子组件暴露给父组件的值属性列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
interface menuData {
    goIndex: () => void;
    getIndexDataList: () => void;
    homeColor: string;
    otherColor: string;
    placeFileColor: string;
    personalColor: string;
}
// ===============================================================================
// 方法

// 页码
export let page = ref<any>(0);
// 子分类id
export let category_id = ref<any>(0);
// 是否请求
export let req = ref<any>(true);
// 文章列表
export let dataList = ref(Array<any>());
// 分类列表
export let categoryList = ref(Array<any>());
// 页码（控制加载下一页是否显示）
export let articlePage = ref(<number>1);
// 下拉刷新标识
export let loading = ref(<boolean>false);
// 搜索值
export let search = ref(<string>"");
// 首页分类菜单默认选中
export let menuActive = ref<number>(0);
// 底部菜单
export let active = ref<number>(0);
// ===============================================================================
// 子组件ref（TypeScript语法）下面这这两种写法也可以，推荐使用Typescript语法
// 子组件menu的对象
export const MenuRef = ref<InstanceType<typeof Menu> & menuData>();
// const MenuRef = ref<InstanceType<typeof Menu>>()
// const MenuRef = ref(null)
/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    onMounted( async()=>{
        const route = useRoute();
        page.value = route.query.page ?? 1;
        category_id.value = route.query.category_id ?? 0;
        req.value = route.query.req ?? true;

        // MenuRef.value = ref<InstanceType<typeof Menu> & menuData>();
        // console.log('获取子组件中的性别', MenuRef.value );
        // console.log('获取子组件中的其他信息', MenuRef.value?.info );
        // console.log('获取子组件中的其他信息', MenuRef.value?.homeColor );
        // 执行子组件方法
        const {dataLists,actives} = await MenuRef.value?.getIndexDataList(page.value);
        // console.log(dataLists.value.articleShow);
        // console.log(actives.value);
        dataList.value = dataLists.value.articleShow;
        categoryList.value = dataLists.value.cateList;
        articlePage.value = dataLists.value.articlePage;
        active.value = actives.value;
    });
    return {MenuRef};
}

/**
 * @name:滚动条回顶部
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const goToTop = () => {
    // 滚动条回顶部
    let json = document.getElementById("body");
    // console.log(json);
    if (json != null) 
    {
        json.scrollTop = 0;
    }
};

/**
 * @name: 下拉刷新方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const onRefresh = async():Promise<void> => {
    let result:any;
    // 执行子组件方法
    const {dataLists,actives} = await MenuRef.value?.getIndexDataList(page.value);
    // console.log(result);
    dataList.value = dataLists.value.articleShow;
    categoryList.value = dataLists.value.cateList;
    articlePage.value = dataLists.value.articlePage;
    active.value = actives.value;
    Toast("刷新成功");
    loading.value = false;
};

/**
 * @name: 跳转文章详情页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 * @param:	category_id	number	分类
 */
export const jumpArticleDetail = (article_id: Number) => {
    router.push({
        path: "/phone/articleDetail",
        query: {
        article_id: utils.encryptCode({ article_id: article_id }),
        path: "index",
        },
    });
};

/**
 * @name: 子组件向父组件抛出的方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const goIndex = (response: any): void => {
    try 
    {
        page.value = response.page;
        category_id.value = response.category_id;
        if (page.value > 1) 
        {
            // 数组合并
            dataList.value.push(...response.articleShow);
        } 
        else 
        {
            // 数组赋值
            dataList.value = response.articleShow;
        }
        categoryList.value = response.cateList;
        articlePage.value = response.articlePage;
    } 
    catch (error) 
    {
        console.log("出错了");
    }
};

/**
 * @name: 获取搜索数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const getSearchData = async():Promise<void> => {
    page.value = 1;
    const {dataLists,actives} = await MenuRef.value?.getIndexDataList(page.value, search.value, category_id.value);
    dataList.value = dataLists.value.articleShow;
    categoryList.value = dataLists.value.cateList;
    articlePage.value = dataLists.value.articlePage;
    active.value = actives.value;
};

/**
 * @name: 加载下一页数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const nextPage = async() => {
    page.value += 1;
    // 调用子组件的方法
    const {dataLists,actives} = await MenuRef.value?.getIndexDataList(page.value, search.value, category_id.value);
    // 循环
    dataLists.value.articleShow.forEach((item:any) => {
        dataList.value.push(item);
    });
    categoryList.value = dataLists.value.cateList;
    articlePage.value = dataLists.value.articlePage;
    active.value = actives.value;
};

/**
 * @name: 获取分类下的文章列表
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const onClickTab = async(obj: number) => {
    const categoryList = ref(Array<any>());
    const articlePage = ref(Array<any>());
    const active = ref<number>(0);
    category_id.value = obj;
    page.value = 1;
    // 调用子组件的方法
    const {dataLists,actives} = await MenuRef.value?.getIndexDataList(page.value, "", category_id.value);
    dataList.value = dataLists.value.articleShow;
    categoryList.value = dataLists.value.cateList;
    articlePage.value = dataLists.value.articlePage;
    active.value = actives.value;
    return {categoryList,articlePage,active};
};

/**
 * @name: nextTick 页面发生变化即渲染
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
nextTick(() => {
/*// 获取子组件name
    console.log(MenuRef._value)
    console.log(MenuRef.__v_isRef)
    console.log(MenuRef.__v_isShallow)
    console.log(MenuRef._rawValue)
    console.log(MenuRef.value.count)
    // 加个问号这种写法，没有属性也不会报错
    console.log(MenuRef.value?.homeColor)
    // 执行子组件方法
    MenuRef.value.goIndex()//*/
});
