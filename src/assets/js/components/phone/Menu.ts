import { reactive, ref, toRefs,onMounted,defineEmits,defineExpose,defineProps } from "vue";
// 引入axios钩子
import axios from "/@/request/axios";
// 引入路由
import { useRouter, useRoute } from "vue-router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// api 接口文件
import {
    getIndexData,
    getRemarkList,
    getFileList,
    getUserSession,
} from "/@/api/phone/index";
import { common, userinfo } from "/@/hooks/common";
import { Toast } from "vant";

const data = reactive({
    // tabbar 默认选中索引
    active: 0,
});

/**
 * @name: 将data绑定值dataRef
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-10
 */
export const { active } = toRefs(data);

/**
 * @name: 调用生命周期onMounted
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2023-01-10
 */
export const Mounted = () => {
    onMounted( ()=>{
        // =============================================
        // 初始调用
        getUserSessionData();
    });
}

/**
 * @name: tabbar 发生改变时
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11
 * @param:	index	number	索引
 */
export const onChange = (index:any): void => { };

/**
 * @name: 获取首页数据公共方法
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11
 * @param:	page	number	页码
 * @param:	search	string	搜索值
 * @param:	category_id	number	分类
 */
export const getIndexDataList = async(
    page: number = 1,
    search: string = "",
    category_id: number = 0,
    sign: boolean = true
): Promise<any> => {
    const dataLists = ref<any>();
    const actives = ref<number>(0);
    // 判断是否为数字
    if (parseFloat(page).toString() == "NaN") {
        page = 1;
    }
    // 请求数据
    // getUserSessionData();
    try {
        Toast.loading({
            message: "加载中...",
            forbidClick: true,
        });
        // utils.alertLoadExec(true);
        let param = {
            page: page,
            search: search,
            category_id: utils.encryptCode({ category_id: category_id }),
        };
        await getIndexData(param).then(function (response: any) {
            response.page = page;
            response.category_id = category_id;
            // console.log(response);
            // 传递数据给父组件
            // $myemit("getIndexDataList", response);
            dataLists.value = response;
            utils.sleep(1500).then(() => {
                // 这里写sleep之后需要去做的事情
                Toast.clear();
            });
        });
    } catch (error) {
        console.log("chucuole");
        // 自定义loading消失
        // utils.alertLoadExec(false);
        Toast.clear();
    }
    actives.value = 0;
    // 返回参数
    return {dataLists,actives}
};

/**
 * @name: 获取杂谈页数据
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11
 * @param:	page	number	页码
 */
export const getOtherData = async(sign: Boolean = true): Promise<any> => {
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    const result = ref<any>();
    const actives = ref<number>(0);
    try {
        let param = {};
        await getRemarkList(param).then(function (response: any) {
            // 传递数据给父组件
            // $myemit("getOtherData", response);
            result.value = response;
            utils.sleep(1500).then(() => {
                // 这里写sleep之后需要去做的事情
                Toast.clear();
            });
        });
    } catch (error) {
        console.log("chucuole");
        Toast.clear();
    }
    actives.value = 1;
    // 返回参数
    return {result,actives};
};

/**
 * @name: 去归档页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11
 */
export const getPlaceFileData = async(sign: Boolean = true): Promise<any> => {
    // utils.alertLoadExec(true);
    Toast.loading({
        message: "加载中...",
        forbidClick: true,
    });
    const result = ref<any>();
    const actives = ref<number>(0);
    // 请求数据
    try {
        let param = {};
        await getFileList(param).then(function (response: any) {
            // 传递数据给父组件
            // $myemit("getPlaceFileData", response);
            result.value = response;
            utils.sleep(5000).then(() => {
                // 这里写sleep之后需要去做的事情
                Toast.clear();
            });
        });
    } catch (error) {
        console.log("chucuole");
        Toast.clear();
    }
    actives.value = 2;
    // 返回参数
    return {result,actives};
};

/**
 * @name: 去个人中心页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11
 */
export const getPersonalData = (): any => {
    // data.active = 3;
    const actives = ref<number>(0);
    actives.value = 3;
    // 返回参数
    return {actives};
};

/**
 * @name: 获取用户登录信息
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const getUserSessionData = (): void => {
    try {
        let param = {};
        getUserSession(param).then(function (response: any) {
            userinfo.email = response.email;
            userinfo.figureurl = response.figureurl;
            userinfo.nickname = response.nickname;
            userinfo.userid = response.userid;
        });
    } catch (error) {
        console.log("登录信息出错！");
    }
};

/**
 * @name: 测试参数传递
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-07-18
 */
export const test = (val:any) => {
    console.log(val)
}
