import {
    PropType,
    ref,
    watch,
    reactive,
    toRefs,
    defineEmits
} from "vue";
// 引入axios钩子
import axios from "/@/request/axios";
// 引入路由
import { useRouter, useRoute } from "vue-router";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
// api 接口文件
import { getIndexData } from "/@/api/phone/index";

// =============================================================================
// 实例化路由
const router = useRouter();
const route = useRoute();

//  使用defineEmits创建名称，接受一个数组(子父组件交互)
const $myemit = defineEmits(['goIndex','goOther','goPlaceFile','goPersonal'])

/**
 * @name: 声明data
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-18 
 */
const data = reactive({
    // 首页图标颜色
    homeColor:'#24657D',
    // 杂谈图标颜色
    otherColor:'#323233',
    // 归档图标颜色
    placeFileColor:'#323233',
    // 我的图标颜色
    personalColor:'#323233',
});

/**
 * @name: 将data绑定值dataRef
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-01-10 
 */
export const { homeColor, otherColor,placeFileColor,personalColor } = toRefs(data);

/**
 * @name: 去首页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11 
 */
export const goIndex = ():void => {
    // 菜单图标变色
    data.homeColor = "#24657D";
    data.otherColor = "#323233";
    data.placeFileColor = "#323233";
    data.personalColor = "#323233";
    // 请求数据
    try 
    {
        getIndexData({}).then(function (response: any) {
            console.log(response);


        });
    } 
    catch (error) 
    {
        console.log('chucuole')
    }


    // 传递数据给父组件
    $myemit('goIndex','新增的数据')
}

/**
 * @name: 去杂谈页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11 
 */
export const goOther = () => {
    data.homeColor = "#323233";
    data.otherColor = "#24657D";
    data.placeFileColor = "#323233";
    data.personalColor = "#323233";
}

/**
 * @name: 去归档页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11 
 */
export const goPlaceFile = () => {
    data.homeColor = "#323233";
    data.otherColor = "#323233";
    data.placeFileColor = "#24657D";
    data.personalColor = "#323233";
}

/**
 * @name: 去个人中心页
 * @author: camellia
 * @email: guanchao_gc@qq.com
 * @date: 2022-08-11 
 */
export const goPersonal = () => {
    data.homeColor = "#323233";
    data.otherColor = "#323233";
    data.placeFileColor = "#323233";
    data.personalColor = "#24657D";
}



// =============================================================================
// 初始方法调用


