import {
    PropType,
    ref,
    watch,
    reactive,
    toRefs,
    provide,
    inject,
    onBeforeMount,// 在组件挂载之前执行的函数
    onMounted,
    onBeforeUpdate,// 在组件修改之前执行的函数
    onUpdated,
    onBeforeUnmount,// 在组件卸载之前执行的函数
    onUnmounted
} from "vue";

import { useRouter } from "vue-router";
import Footer from "/@/components/pc/Footer.vue";
import Header from "/@/components/pc/Header.vue";
import Menu from "/@/components/pc/Menu.vue";
import load from "/@/components/pc/loading.vue";
import TopIM from "/@/components/pc/TopIM.vue";
import Drawer from "/@/components/pc/Drawer.vue";
// 引入公共js文件
import utils from "/@/assets/js/public/function";
import { nprogressStart, nprogressClose } from "/@/plugin/nprogress";
// api 接口文件
import { clickOKExcel } from "/@/api/pc/tools";
// 文件上传
import { ElMessage, ElMessageBox } from 'element-plus'
import type { UploadProps, UploadUserFile } from 'element-plus'
export default {
    name: "label",
    components: {
        Footer,
        Header,
        Menu,
        load,
        TopIM,
        Drawer,
    },
    // VUE3 语法 第一个执行的钩子函数
    // setup官方文档
    // https://www.vue3js.cn/docs/zh/guide/composition-api-setup.html#参数
    setup(props: any, content: any) {
        // 实例化路由
        var router = useRouter();
        /**
         * @name: 全局变量
         * @author: camellia
         * @email: guanchao_gc@qq.com
         * @date: 2020-12-24 
         */
        const data = reactive({
            showRef: 0,
            loading: true,
            list:[],
            column:'',
            start:'',
            end:'',
            fileList:
            [
                // {
                //     name: 'element-plus-logo.svg',
                //     url: 'https://element-plus.org/images/element-plus-logo.svg',
                // },
            ],
            // excel 文件路径
            fileUrl:'',
            sheet:"",

        });

        /**
         * @name: 获取标签数据
         * @author: camellia
         * @email: guanchao_gc@qq.com
         * @date: 2021-01-06 
         */
        const getData = () => {
            //  http://www.axios-js.com/zh-cn/docs/
            nprogressStart();
            data.loading = true;
            data.loading = false;
            nprogressClose();
        }

        /**
         * 删除
         * @param file 
         * @param uploadFiles 
         */
        const handleRemove: UploadProps['onRemove'] = (file, uploadFiles) => {
            console.log(file, uploadFiles)
        }
        
        const handlePreview: UploadProps['onPreview'] = (uploadFile) => {
            console.log(uploadFile)
        }
        
        const handleExceed: UploadProps['onExceed'] = (files, uploadFiles) => {
            ElMessage.warning(
                `请点击删除当前文件再执行上传！`
            )
        }
        /**
         * 删除前
         * @param uploadFile 
         * @param uploadFiles 
         * @returns 
         */
        const beforeRemove: UploadProps['beforeRemove'] = (uploadFile, uploadFiles) => {
            return ElMessageBox.confirm(
                `Cancel the transfer of ${uploadFile.name} ?`
            ).then(
                () => true,
                () => false
            )
        }
        /**
         * 上传成功
         * @param response 
         * @param uploadFile 
         * @param uploadFiles 
         */
        const uploadSuccess: UploadProps['onSuccess'] = (response, uploadFile, uploadFiles) => {
            console.log(response)
            if(response.code > 0)
            {
                data.fileUrl = response.name;
            }
        }

        const clickOK = () => {    
            nprogressStart();
            // 开启自定义loading（返回一个html节点）
            utils.alertLoadExec(true);
            if(data.column == '' || data.start == '' || data.end == '' || data.fileUrl == '' || data.sheet == '' )
            {
                // 关闭自定义loading（需要将创建时的节点传入）
                utils.alertLoadExec(false);
                nprogressClose();
                utils.alertMsg(2000, "请填写全部参数！");
                return;
            }
            let info = {
                column: data.column,
                end: data.end,
                start: data.start,
                fileUrl: data.fileUrl,
                sheet:data.sheet
            };
            try {
                clickOKExcel(info).then(function (response: any) {
                    console.log(response)
                    // 关闭自定义loading（需要将创建时的节点传入）
                    utils.alertLoadExec(false);
                    nprogressClose();
                    if(response.code < 0)
                    {
                        utils.alertMsg(2000, response.msg);
                        return false;
                    }
                    return ElMessageBox.confirm(
                        ` ${data.column} 列的数据统计总和为：${response.number}`
                    ).then(
                        () => {
                            // data.fileList = [];
                        },
                        () => false
                    )
                });
            } catch (error) {
                utils.alertLoadExec(false);
                utils.alertMsg(2000, '系统错误');
            }
        }

        // ===================================================================
        /**
         * @name: 右上角菜单
         * @author: camellia
         * @email: guanchao_gc@qq.com
         * @date: 2021-01-06 
         */
        const closeMenu = (param: number) => {
            // param就是子组件传过来的值
            data.showRef = param;
        }
        const showMenuByChild = (param: number) => {
            data.showRef = param;
        }
        // ===================================================================

        // ===================================================================
        // 初始调用
        getData();

        /**
         * @name: 将data绑定值dataRef
         * @author: camellia
         * @email: guanchao_gc@qq.com
         * @date: 2021-01-10 
         */
        const dataRef = toRefs(data);
        return {
            showMenuByChild,
            closeMenu,
            handleRemove,
            handlePreview,
            handleExceed,
            beforeRemove,
            uploadSuccess,
            clickOK,
            ...dataRef
        }
    },
    methods: {
    }
};

