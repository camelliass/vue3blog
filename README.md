# blogVueTs

## 安装依赖
```
yarn
```

### 运行项目
```
yarn dev
```

### 项目打包
```
yarn build
```

项目部署线上实例：https://guanchao.site

当前项目PC端使用VUE3.0语法开发，手机端使用VUE3.2语法开发。

手机端网站已打包成APP，APP下载链接：https://resource.guanchao.site/shijianlide.apk

PC端项目中未使用组件组，弹窗、loading、分页等组件均为自定义封装。手机端使用vant组件库。

欢迎留下您的宝贵建议。
